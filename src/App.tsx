import { Component, createEffect, createMemo, createSignal, on, Setter, Signal } from 'solid-js';
import Emgality from "./assets/videos/Video_Emgality.mp4";
import Migrane from "./assets/videos/Video_Migraine.mp4";
import { Button } from "./components/Button"

const App: Component = () => {

  const [state, setState] = createSignal(true);
  const [video, setVideo] = createSignal<HTMLVideoElement>();

  createEffect(on(state, () => {
    if (video()) {
      video().animate(
        [
          { opacity: 1, },
          { opacity: 0, }
        ],
        { duration: 1000, fill: "forwards", easing: "ease-in" }
      ).finished.then(
        (animation) => {
          video().src = state() ? Emgality : Migrane
          video().muted = false;
          video().loop = true;
          video().volume = 1;
          animation.reverse();
        }
      )
    }
  }))

  return (
    <>
      <div class='absolute min-w-full min-h-full'>
        <video
          class='absolute min-w-full min-h-full z-10'
          ref={setVideo} autoplay muted loop src={Emgality}
        />
        <div class='absolute z-30 w-1/6 bottom-16 right-1/2 translate-x-1/2 '>
          <Button state={[state, setState]} />
        </div>
      </div >
      <div class='fixed left-0 bottom-0 p-4 z-20 text-black text-sm text-left font-normal antialiased'>
        <div>PP-GZ-RO-0114</div>
        <div>Acest material este destinat profesioniștilor în domeniul sănătății.</div>
      </div>
    </>

  );
};

export default App;

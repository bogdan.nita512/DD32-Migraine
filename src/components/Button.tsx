import { Component, createSignal, Signal } from "solid-js";
import Start from "../assets/buttons/Button_Start.png";
import Stop from "../assets/buttons/Button_Stop.png";

export const animation = (element: Element) => element.animate(
    [{ opacity: 1, scale: 1 }, { opacity: 0, scale: 1.5 }],
    { duration: 1000, fill: "forwards", easing: "ease-in" }
)

export const Button: Component<{ state: Signal<boolean> }> =
    (props) => {
        const [button, setButton] = createSignal<HTMLButtonElement>()
        const [image, setImage] = createSignal<HTMLImageElement>()

        const [state, setState] = props.state;

        const toggle = () => {
            button().disabled = true;
            animation(image()).finished.then((anim) => {
                setState((value) => !value)

                image().src = state() ? Start : Stop
                setTimeout(() => {
                    anim.reverse()
                    anim.finished.then(() => { button().disabled = false })
                }, 2500)
            })
        }


        return (
            <button
                ref={setButton}
                class='outline-none'
                onClick={() => toggle()}
                onTouchEnd={() => toggle()}
            >
                <img ref={setImage} src={Start} />
            </button>
        )
    }